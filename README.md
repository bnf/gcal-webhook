# Library webhooks

Library webhook features parsing of Google Calendar Events

Forked from <https://github.com/go-playground/webhooks> which is focused on git based json apis

Open to expansion and inclusion of other services

## Installation

Use go get.

```shell
go get -u gitlab.com/bnf/gcal-webhook
```

Then import the package into your own code.

```shell
    import "gitlab.com/bnf/gcal-webhook/google-calendar"
```

## Example

```go
package main

import (
  "fmt"

  "net/http"

  gcwebhook "gitlab.com/bnf/gcal-webhook"
  gcal "google.golang.org/api/calendar/v3"
)

const (
  path = "/webhooks"
)

var (
  svc        *gcal.Service
  hook       *gcwebhook.Webhook
  path    = "/webhooks/google_calendar"
  dFormat = "Mon Jan 2"
  tFormat = "3:04pm"
)

func responseHandler(_ http.ResponseWriter, r *http.Request) {

  // TODO: actually, all of the event will parse into the same struct, so this is dissingenous, should fix the webhooks library
  payload, err := hook.Parse(r, gcwebhook.ExistsEvent)
  if err != nil {
    log.Error(err)
    return
  }
  gc := payload.(*gcwebhook.Payload)
  log.Infof("gc: %+v", gc)

  msg := ""
  switch gc.ResourceState {
  case string(gcwebhook.NotExistsEvent): // deletion
    // not exists is a deletion, which is harder to track
    // t := time.Unix(int64(dbuild.PushData.PushedAt), 0)
    msg = fmt.Sprintf("Google Calendar `%s` has changed %s (probably a deletion) %s",
      w.Calendar.Summary,
      gc.ResourceURI,
      gc.ChannelExpiration.Format(dFormat))

  case string(gcwebhook.ExistsEvent): // new items
    events, err := retrieveEvents(w)
    if err != nil {
      log.Error(err)
      msg += "webhook triggered, problem retrieving events"
    } else {
      // log.Infof("events: %+v", events)
      if len(events.Items) > 0 {
        msg += w.Calendar.Summary + ": "
        for i, event := range events.Items {
          if event != nil {
            if len(events.Items) > 1 {
              msg += "\n"
            }
            if event.Status == "cancelled" && i < len(events.Items) {
              // gosh it'd be nice to mention something but they don't send no info along
              // event.Start = event.OriginalStartTime
              // msg += " CANCELLED " + event.
              // TODO: we could use the Etag which is offered to bactrack to a corpus which we keep (ugh) but I don't want to store that stuff
              log.Infof("cancelled event %+v", event)
              return
            }
            msg += formatEvent(event)
          }
        }
      }
    }
  case string(gcwebhook.SyncEvent): // new items
    msg += w.Calendar.Summary + ": "
    _, err := retrieveEvents(w)
    if err != nil {
      log.Error(err)
      msg += "webhook triggered, problem syncronizing events"
    } else {
      msg += "events synchronized"
    }
  }
}

```

## Contributing

Pull requests for other services are welcome!

If the changes being proposed or requested are breaking changes, please create an issue for discussion.

## License

Distributed under MIT License, please see license file in code for more details.
